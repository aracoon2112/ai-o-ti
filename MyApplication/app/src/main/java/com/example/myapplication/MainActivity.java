package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import  com.hoho.android.usbserial.*;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;


import android.content.Context;
import android.hardware.usb.*;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.lang.Math.*;
public class MainActivity extends AppCompatActivity {

    public int decCount =0;
    public boolean dotted = false;
    enum STATE  {
            START,
            FIRST_OP,
            WAIT_SECOND_OP,
            SECOND_OP
    }
    STATE state = STATE.START;
    public double tempResult ;
    public double operand1 =0;
    public double operand2 = 0;
    public long operand1_int =0;
    public long operand1_dec =0;
    public long operand2_int = 0;
    public  long operand2_dec =0;
    public String operator ="";

    public boolean flag = false;
    TextView txtResult;
    Button  btnAdd,one , two, three , four , five , six , seven , eight , nine , zero, equal , add , subtract, divide , multiply, AC , DEL , percent , dotButton;
    protected void setHandler(Button b){
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(b.getText().toString());
            }
        });
    }
    double dec(int time){
        double one =1 ;
        for (int i =0 ; i<time;i++){
            one /=10;
        }
        return one;
    }
    double calc(){

        System.out.println(operator);
        if (operator.length()>0) {
            if (operator.contains("+")) {
                return operand1 + operand2;
            } else if (operator.contains("-")) {
                return operand1 - operand2;
            } else if (operator.contains("/")) {
                return operand1 / operand2;
            } else if (operator.contains("x")) {
                return operand1 * operand2;
            }
        }
        return operand2> 0 ? operand1:operand2;
    }

    protected void stateTrasition(String input){
        String operators = "-x+=%/.";
        String digit = "0123456789";
        String dot = ".";
        switch (state){
            case START :
                if (operators.contains(input)){
                    state= STATE.WAIT_SECOND_OP;
                } else if (digit.contains(input)){
                    txtResult.setText(input);
                    operand1 = Double.parseDouble(input);
                    state= STATE.FIRST_OP;
                }
                break;
            case FIRST_OP:
                if (operators.contains(input)){
                    if (input.contains("%")){

                        operand1 = operand1/100;
                        txtResult.setText(Integer.toString((int)operand1));
                    } else if (input.contains(dot)){
                        if (!dotted){
                            dotted = true ;
                            txtResult.setText(Integer.toString((int)operand1)+".");
                        }
                    }
                    else {
                        dotted = false;
                        decCount = 0;
                        state = STATE.WAIT_SECOND_OP;
                        operator = input;
                    }
                } else if (digit.contains(input)){
                    if (dotted){
                        decCount++;
                        operand1+=Double.parseDouble(input)*dec(decCount);
                        txtResult.setText(Double.toString(operand1));
                    } else{
                        operand1 = operand1*10f +Double.parseDouble(input);
                        state = STATE.FIRST_OP;
                        txtResult.setText(Integer.toString((int)operand1));
                    }

                 } else if (input.contains("DEL")) {
                    txtResult.setText("0");
                    operand1 =0 ;
                    dotted =false ;
                    decCount =0 ;
                    state = STATE.START;
                } else if (input.contains("AC"))
                {
                    operand1 =0 ;
                    operand2 = 0 ;
                    operator ="" ;
                    dotted = false ;
                    decCount = 0 ;
                    txtResult.setText("0");
                    state = STATE.START;
                }
                break;
            case WAIT_SECOND_OP:
                if (operators.contains((input))){
                    operator = input ;
                    state  = STATE.WAIT_SECOND_OP;

                } else if (digit.contains(input)){
                    txtResult.setText(input);
                    operand2 = Double.parseDouble(input);
                    state = STATE.SECOND_OP;

                } else if (input.contains("AC") || input.contains("DEL"))  {
                    operand2 = 0;
                    operand1 = 0;
                    operator = "";
                    txtResult.setText("0");
                    state = STATE.START;
                }
                break;
            case SECOND_OP:
                if (operators.contains(input)){
                    if (input.contains("%")){
                        operand2=operand2/100;
                    } else if (input.contains(".")){
                        if (!dotted){
                            dotted = true ;
                            decCount = 0;
                            txtResult.setText(Double.toString(operand2)+".");
                        }
                    } else {
                        operand1 = calc();
                        if (input.contains("=")){

                            operand2 = 0;
                            txtResult.setText((Double.toString(operand1)));
                            state= STATE.FIRST_OP;


                        } else {
                            operand2 =0 ;
                            txtResult.setText(Double.toString(operand1));
                            state = STATE.WAIT_SECOND_OP;
                        }

                    }


                } else if (digit.contains(input)){
                    if (dotted){
                        decCount++;
                        operand2 += Double.parseDouble(input) * dec(decCount);
                        state = STATE.SECOND_OP;
                    } else {
                        operand2 = operand2 * 10 + Double.parseDouble(input);
                        txtResult.setText(Integer.toString((int)operand2));
                        state = STATE.SECOND_OP;
                    }
                } else if (input.contains("DEL")) {
                    operand2  =  0;
                    dotted = false ;
                    decCount = 0 ;
                    txtResult.setText(Double.toString(operand1));
                    state = STATE.WAIT_SECOND_OP;

                } else {
                    txtResult.setText("0");
                    state = STATE.START;
                }
                break;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtResult = findViewById(R.id.result);
        one =findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        six = findViewById(R.id.six);
        seven = findViewById(R.id.seven);
        eight = findViewById(R.id.eight);
        nine = findViewById(R.id.nine);
        zero = findViewById(R.id.zero);
        equal = findViewById(R.id.equalButton);
        add = findViewById(R.id.addButton);
        subtract = findViewById(R.id.subtractButton);
        divide = findViewById(R.id.divButton);
        multiply = findViewById(R.id.multiplyButton);
        AC = findViewById(R.id.ACButton);
        DEL = findViewById(R.id.DELButton);
        percent = findViewById(R.id.percentButton);
        dotButton = findViewById(R.id.dot);

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(zero.getText().toString());
            }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(one.getText().toString());
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    stateTrasition(two.getText().toString());
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(three.getText().toString());
            }

        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(four.getText().toString());
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(five.getText().toString());
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(six.getText().toString());
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(seven.getText().toString());
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(eight.getText().toString());
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateTrasition(nine.getText().toString());
            }
        });
        setHandler(equal);
        setHandler(add);
        setHandler(divide);
        setHandler(subtract);
        setHandler(multiply);
        setHandler(AC);
        setHandler(DEL);
        setHandler(percent);
        setHandler(dotButton);
    }


}