package com.example.mqttserver;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.nio.charset.Charset;


public class MainActivity extends AppCompatActivity {
    MQTTHelper mqttHelper;
    String logText ="";
//    ScrollView scrollView;
    TextView scrollView;
  //  TextView result  = new TextView(this);
    Button button;
    EditText editText;
    private void sendDataToMQTT(String ID, String value){

        MqttMessage msg = new MqttMessage(); msg.setId(1234);
        msg.setQos(0); msg.setRetained(true);

        String data = ID + ":" + value;

        byte[] b = data.getBytes(Charset.forName("UTF-8"));
        msg.setPayload(b);

        try {
            mqttHelper.mqttAndroidClient.publish("NPNLab_BBC/feeds/bbc-led", msg);

        }catch (MqttException e){
        }
    }
    private void addView(String text,int color){
        int start = scrollView.getText().length();
        scrollView.append(text);
        int end = scrollView.getText().length();
        Spannable spannableText = (Spannable) scrollView.getText();
        spannableText.setSpan(new ForegroundColorSpan(color), start, end, 0);
        if (scrollView.getLineCount()>= scrollView.getMaxLines()){

        }
        scrollView.append("\n");
    }
    private void startMQTT(){
        mqttHelper = new MQTTHelper(getApplicationContext());

        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                addView("[event] Connected",Color.GREEN);

            }

            @Override
            public void connectionLost(Throwable throwable) {
                addView("[event] Disconnected", Color.RED);
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
               // Log.d("app", "messageArrived: "+topic);
                try {
                    addView("[message] topic : " +topic +"\t payload"+(new String(mqttMessage.getPayload(),"utf8")),Color.WHITE);
                } catch (Exception e){}

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                MqttMessage mess;
                String[] topic;
                try {
                    mess= iMqttDeliveryToken.getMessage();
//                    topic =topic iMqttDeliveryToken.getTopics();
                    try {
//                        Log.d("del", topic[0]);
                        addView("[published]  payload: "+ (new String(mess.getPayload(),"utf-8")),Color.BLUE);
                    } catch (Exception e){}

                } catch (MqttException e){
                    Log.d("app", "error ");
                }

            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scrollView = findViewById(R.id.scrollContent);
        scrollView.setMaxLines(2000);
        button = findViewById(R.id.button);
        editText = findViewById(R.id.textInput);
        Log.d("app", "onCreate: ec");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                    sendDataToMQTT(editText.getText().toString(), "test");

                editText.setText("");
            }

        });
        startMQTT();
    }

}